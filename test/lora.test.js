'use strict';

const expect = require('chai').expect;
const translators = require('../src');

function expectLastActive (val) {
  expect(val).to.have.any.key('lastActive');
  expect(val.lastActive).to.be.an('number');
}

describe('lora', function () {
  describe('rhf1s001', function () {
    const translator = translators.getTranslator('lora-node-rhf1s001', 'simple-lora-node');
    it('should parse valid data', function (done) {
      translator.translate({value: {data: 'AbhjlZYA///I'}})
      .then((val) => {
        expectLastActive(val);
        delete val.lastActive;
        expect(val).to.eql({
          value: {
            temperature: 21.6,
            humidity: 66.75,
            battery: 3.5,
            rssi: 75
          }
        });
        done();
      }).catch(err => done(err));
    });
  });
  describe('arf8123aa', function () {
    const translator = translators.getTranslator('lora-node-arf8123aa', 'simple-lora-node');
    it('should parse valid data', function (done) {
      translator.translate({value: {data: 'nhRVQpaQATE0ABISDxU='}})
      .then((val) => {
        expectLastActive(val);
        delete val.lastActive;
        expect(val).to.eql({
          value: {
            temperature: 20,
            battery: 3.861,
            GPSMetaData: { hdop: 1, satellites: 2 },
            rssi: undefined,
            snr: undefined
          },
          latlng: [55.71615, 13.223333333333333]
        });
        done();
      }).catch(err => done(err));
    });
  });
});
