'use strict';

const expect = require('chai').expect;
const yggioTranslators = require('../src');

describe('general functions', function () {
  it('should be possible to get available translators', function (done) {
    yggioTranslators.getAvailableTranslators('lora-node', 'rhf1s001')
    .then((translators) => {
      expect(translators).to.be.an('array');
      expect(translators).to.have.lengthOf(3);
      done();
    }).catch(err => done(err));
  });
  it('should be possible to get all available translators', function (done) {
    yggioTranslators.getAllTranslators()
    .then((translators) => {
      expect(translators).to.be.an('array');
      expect(translators).to.have.lengthOf.above(0);
      done();
    }).catch(err => done(err));
  });
});
