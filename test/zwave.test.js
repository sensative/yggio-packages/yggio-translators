'use strict';

const expect = require('chai').expect;

// let amp = { // Current value
//   'descr': 'meter',
//   'unit': '5',
//   'type': '1',
//   'precision': '3',
//   'dataSize': '4',
//   'data': '0x00000006',
//   'deltaTime': '0',
//   'prevData': '0x00000000',
//   'rateType': '1',
//   'data32': '0x00000006',
//   'prevData32': '0x00000000'
// };
let kwh = { // Power-usage value
  'meter': {
    'node': '62',
    'instance': '0',
    'vid': '0x0159',
    'ptype': '0x0001',
    'pid': '0x0051',
    'class': '0x0032',
    'descr': 'meter',
    'unit': '0',
    'type': '1',
    'precision': '1',
    'dataSize': '4',
    'data': '0x000000d3',
    'deltaTime': '1',
    'prevData': '0x00000000',
    'rateType': '1',
    'data32': '0x00000000',
    'prevData32': '0x00000000'
  }
};
let tempVal = {
  'node': '29',
  'instance': '0',
  'vid': '0x013c',
  'ptype': '0x0002',
  'pid': '0x0020',
  'class': '0x0031',
  'descr': 'msensor',
  'type': '0x01',
  'precision': '2',
  'unit': '1',
  'size': '2',
  'data': '0x1b33',
  'data32': '0x1b330000'
};
let humVal = {
  'node': '29',
  'instance': '0',
  'vid': '0x013c',
  'ptype': '0x0002',
  'pid': '0x0020',
  'class': '0x0031',
  'descr': 'msensor',
  'type': '0x05',
  'precision': '2',
  'unit': '0',
  'size': '2',
  'data': '0x1336',
  'data32': '0x13360000'
};
const translators = require('../src');
const meter = translators.getTranslator('zwave-power-meter', 'fiware-value');
const msensor = translators.getTranslator('zwave-temp-humidity', 'temp-humidity');
describe('Zwave to FIWARE', function () {
  it('zwave powermeter should parse valid data', function (done) {
    meter.translate(kwh).then((val) => {
      expect(val).to.eql({
        value: 21.1,
        unitText: 'kWh'
      });
      done();
    }).catch(err => done(err));
  });
  // it('zwave powermeter should NOT parse invalid data', function (done) {
  //   meter.translate(amp).then(() => {}, (err) => {
  //     expect(err).to.be.an('error');
  //     done();
  //   });
  // });
  it('zwave temperature should parse valid data', function (done) {
    msensor.translate(tempVal).then((val) => {
      done();
    }).catch(err => done(err));
  });
  it('zwave Humidity should parse valid data', function (done) {
    msensor.translate(humVal).then((val) => {
      done();
    }).catch(err => done(err));
  });
});
