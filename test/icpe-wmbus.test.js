'use strict';

const expect = require('chai').expect;
const translators = require('../src');

describe('icpe-wmbus', function () {
  describe('generic-wmbus-power-meter', function () {
    const translator = translators.getTranslator('icpe-wmbus-generic-meter', 'fiware-device');
    it('should parse valid data', function (done) {
      translator.translate({value: {data: 12345, vif: '03'}})
      .then((val) => {
        expect(val).to.eql({
          value: 12.345,
          unitText: 'kWh'
        });
        done();
      }).catch(err => done(err));
    });
  });

  describe('generic-wmbus-power-meter', function () {
    const translator = translators.getTranslator('icpe-wmbus-generic-meter', 'fiware-device');
    it('should parse valid data', function (done) {
      translator.translate({value: {data: 12345, vif: '06'}})
      .then((val) => {
        expect(val).to.eql({
          value: 12345,
          unitText: 'kWh'
        });
        done();
      }).catch(err => done(err));
    });
  });
});
