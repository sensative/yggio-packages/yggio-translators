'use strict';

require('./general-functions.test');
require('./icpe-wmbus.test');
require('./lora.test');
require('./zwave.test');
