'use strict';

const _ = require('lodash');

const defaultValidator = (input, value) => {
  if (input.path) {
    return _.get(value, input.path);
  } else {
    return value;
  }
};

const regex = (input, value) => {
  let inputValue = _.get(value, input.path);
  let filter = _.get(input, 'validate.filter');
  let validated = new RegExp(filter).test(inputValue);
  if (validated) {
    return Promise.resolve(inputValue);
  } else {
    return Promise.reject(new Error(inputValue + ' does not match ' + filter + ' at ' + input.path));
  }
};

const validators = {regex};

const validate = (input, value) => {
  const validateFunc = _.get(validators, _.get(input, 'validate.type'), defaultValidator);
  return validateFunc(input, value);
};

module.exports = {
  validate
};
