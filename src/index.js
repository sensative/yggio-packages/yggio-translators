'use strict';

const _ = require('lodash');
const all = _.concat([], require('./translators/zwave'), require('./translators/lora-node'), require('./translators/icpe-wmbus'));
const Translator = require('./translator');

function getAllTranslators () {
  const translatorSpecs = _.map(all, (translatorSpec) => {
    const translators = _.map(translatorSpec.translators, (translator) => {
      return _.pick(translator, ['toName', 'inputs']);
    });
    return {
      fromName: translatorSpec.fromName,
      translators: translators
    };
  });
  return Promise.resolve(translatorSpecs);
}

function getAvailableTranslators (iotnodeNodeType, modelName) {
  const translatorSpec = _.find(all, function (translatorSpec) {
    return translatorSpec.fromName === (iotnodeNodeType + '-' + modelName);
  }) || [];
  let translators = _.map(translatorSpec.translators, (translator) => {
    return _.pick(translator, ['toName', 'inputs']);
  });
  if (!translators) translators = [];
  return Promise.resolve(translators);
}

function getTranslator (fromName, toName) {
  const translatorSpec = _.find(all, {
    'fromName': fromName
  });
  if (translatorSpec) {
    const translator = _.find(translatorSpec.translators, {
      'toName': toName
    });
    if (translator) return new Translator(translator);
  }
  return null;
}

module.exports = {
  all: all,
  getAllTranslators,
  getAvailableTranslators,
  getTranslator
};
