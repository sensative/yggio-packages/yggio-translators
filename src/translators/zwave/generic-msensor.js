'use strict';
const _ = require('lodash');
// zwave meter parser module
const fiware = {
  toName: 'fiware-device',
  inputs: {
    'data': {
      path: 'data',
      validate: {
        type: 'regex',
        filter: '0x[aA-fF0-9]+'
      }
    },
    'type': {
      path: 'type',
      validate: {
        type: 'regex',
        filter: '0x[aA-fF0-9]+'
      }
    },
    'unit': {
      path: 'unit',
      validate: {
        type: 'regex',
        filter: '[0-9]'
      }
    },
    'precision': {
      path: 'precision',
      validate: {
        type: 'regex',
        filter: '[0-9]'
      }
    }
  },
  translateFunction: function (data, type, unit, precision) {
    return {
      value: parseInt(data) / Math.pow(10, parseInt(precision)),
      name: _.get(types, type).name,
      unitText: _.get(_.get(types, type), 'units.' + unit)
    };
  }
};
const types = {
  '0x00': {
    name: 'Reserved',
    units: {}
  },
  '0x01': { // TODO find out why meter does not rapport according to zwave spec
    name: 'AirTemperature',
    units: {
      '0': 'C',
      '1': 'F'
    }
  },
  '0x03': {
    name: 'Luminance',
    units: {
      '0': '%',
      '1': 'lux'
    }
  },
  '0x05': {
    name: 'Humidity',
    units: {
      '0': '%',
      '1': 'g/m3'
    }
  },
  '0x1f': {
    name: 'Moisture',
    units: {
      '0': '%',
      '1': 'g/m3'
    }
  }
};

module.exports = {
  fromName: 'zwave-generic-msensor',
  translators: [
    fiware
  ]
};
