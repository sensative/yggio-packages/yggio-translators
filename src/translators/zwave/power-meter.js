const meter = require('./generic-meter');
const _ = require('lodash');
const fiware = {
  toName: 'fiware-value',
  inputs: _.merge(meter.translators[0].inputs, {
    'type': {
      path: 'meter.type',
      validate: {
        type: 'regex',
        filter: '1'
      }
    },
    'unit': {
      path: 'meter.unit',
      validate: {
        type: 'regex',
        filter: '0'
      }
    }
  }),
  translateFunction: meter.translators[0].translateFunction
};

module.exports = {
  fromName: 'zwave-power-meter',
  translators: [
    fiware
  ]
};
