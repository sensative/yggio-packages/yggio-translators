const msensor = require('./generic-msensor');
const _ = require('lodash');
const tempHumidity = {
  toName: 'temp-humidity',
  inputs: _.merge(msensor.translators[0].inputs, {
    type: {
      validate: {
        filter: '0x01|0x05'
      }
    }
  }),
  translateFunction: function (data, type, unit, precision) {
    let val = msensor.translators[0].translateFunction(data, type, unit, precision);
    if (val.unitText === 'F') { // DO Fahrenheit to Celsius
      val.value = (val.value - 32) * 5 / 9;
      let scale = Math.pow(10, parseInt(precision));
      val.value = Math.round(val.value * scale) / scale;
      val.unitText = 'C';
    }
    return val;
  }
};

module.exports = {
  fromName: 'zwave-temp-humidity',
  translators: [
    tempHumidity
  ]
};
