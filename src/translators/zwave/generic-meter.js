'use strict';
const _ = require('lodash');
// zwave meter parser module
const fiware = {
  toName: 'fiware-device',
  inputs: {
    'data': {
      path: 'meter.data',
      validate: {
        type: 'regex',
        filter: '0x[aA-fF0-9]+'
      }
    },
    'type': {
      path: 'meter.type',
      validate: {
        type: 'regex',
        filter: '[0-9]'
      }
    },
    'unit': {
      path: 'meter.unit',
      validate: {
        type: 'regex',
        filter: '[0-9]'
      }
    },
    'precision': {
      path: 'meter.precision',
      validate: {
        type: 'regex',
        filter: '[0-9]'
      }
    }
  },
  translateFunction: function (data, type, unit, precision) {
    return {
      value: parseInt(data) / Math.pow(10, parseInt(precision)),
      unitText: _.get(_.get(types, type), 'units.' + unit)
    };
  }
};
const types = {
  '0': {
    name: 'Reserved',
    units: {}
  },
  '11': { // TODO find out why meter does not rapport according to zwave spec
    name: 'Single-E electric meter',
    units: {
      '0': 'kWh',
      '1': 'kVARh',
      '2': '%',
      '3': 'Pulse count',
      '4': 'kVAR',
      '5': 'Voltage (V)',
      '6': 'Amperes (A)',
      '7': 'kW',
      '8': 'Ratio'
    }
  },
  '2': {
    name: 'Gas meter',
    units: {
      '0': 'Cubic meter',
      '1': 'Cubic feet',
      '2': 'US gallon',
      '3': 'Pulse count',
      '4': 'IMP gallon',
      '5': 'Liter',
      '6': 'kPa',
      '7': 'Centum cubic feet',
      '8': 'Cubic meter per hour',
      '9': 'Liter per hour',
      '10': 'kWh',
      '11': 'MWh',
      '12': 'KW',
      '13': 'Hours'
    }
  },
  '3': {
    name: 'Water meter',
    units: {
      '0': 'Cubic meter',
      '1': 'Cubic feet',
      '2': 'US gallon',
      '3': 'Pulse count',
      '4': 'IMP gallon',
      '5': 'Liter',
      '6': 'kPa',
      '7': 'Centum cubic feet',
      '8': 'Cubic meter per hour',
      '9': 'Liter per hour',
      '10': 'kWh',
      '11': 'MWh',
      '12': 'KW',
      '13': 'Hours'
    }
  },
  '4': {
    name: 'Twin-E electric meter',
    units: {
      '0': 'kWh',
      '1': 'kVARh',
      '2': '%',
      '3': 'Pulse count',
      '4': 'kVAR',
      '5': 'Voltage (V)',
      '6': 'Amperes (A)',
      '7': 'kW',
      '8': 'Ratio'
    }
  },
  '5': {
    name: '3P Single Direct electric meter',
    units: {
      '0': 'kWh',
      '1': 'kVARh',
      '2': '%',
      '3': 'Pulse count',
      '4': 'kVAR',
      '5': 'Voltage (V)',
      '6': 'Amperes (A)',
      '7': 'kW',
      '8': 'Ratio'
    }
  },
  '6': {
    name: '3P Single ECT electric meter',
    units: {
      '0': 'kWh',
      '1': 'kVARh',
      '2': '%',
      '3': 'Pulse count',
      '4': 'kVAR',
      '5': 'Voltage (V)',
      '6': 'Amperes (A)',
      '7': 'kW',
      '8': 'Ratio'
    }
  },
  '7': {
    name: '1 Phase Direct Electricity Meter',
    units: {
      '0': 'kWh',
      '1': 'kVARh',
      '2': '%',
      '3': 'Pulse count',
      '4': 'kVAR',
      '5': 'Voltage (V)',
      '6': 'Amperes (A)',
      '7': 'kW',
      '8': 'Ratio'
    }
  },
  '8': {
    name: 'Heating meter',
    units: {
      '0': 'Cubic meter (m3)',
      '1': 'Metric Ton (tonne) (t)',
      '2': 'Cubic meter per hour(m3/h)',
      '3': 'Liter per hour (l/h)',
      '4': 'kW',
      '5': 'MW',
      '6': 'kWh',
      '7': 'MWh',
      '8': 'Giga Joule (GJ)',
      '9': 'Giga Calorie (Gcal)',
      '10': 'Celsius (C)',
      '11': 'Fahrenheit (F)',
      '12': 'Hours'
    }
  },
  '9': {
    name: 'Cooling meter',
    units: {
      '0': 'Cubic meter (m3)',
      '1': 'Metric Ton (tonne) (t)',
      '2': 'Cubic meter per hour(m3/h)',
      '3': 'Liter per hour (l/h)',
      '4': 'kW',
      '5': 'MW',
      '6': 'kWh',
      '7': 'MWh',
      '8': 'Giga Joule (GJ)',
      '9': 'Giga Calorie (Gcal)',
      '10': 'Celsius (C)',
      '11': 'Fahrenheit (F)',
      '12': 'Hours'
    }
  },
  '10': {
    name: 'Combined Heating and Cooling Meter',
    units: {
      '0': 'Cubic meter (m3)',
      '1': 'Metric Ton (tonne) (t)',
      '2': 'Cubic meter per hour(m3/h)',
      '3': 'Liter per hour (l/h)',
      '4': 'kW',
      '5': 'MW',
      '6': 'kWh',
      '7': 'MWh',
      '8': 'Giga Joule (GJ)',
      '9': 'Giga Calorie (Gcal)',
      '10': 'Celsius (C)',
      '11': 'Fahrenheit (F)',
      '12': 'Hours'
    }
  },
  '1': {
    name: 'Electric Sub-Meter',
    units: {
      '0': 'kWh',
      '1': 'kVAh',
      '2': 'W',
      '3': 'Pulse Count',
      '4': 'V',
      '5': 'A',
      '6': 'Power Factor (%)'
    }
  }
};

module.exports = {
  fromName: 'zwave-generic-meter',
  translators: [
    fiware
  ]
};
