'use strict';

function rawTranslate (payLoad) {
  return JSON.parse((Buffer.from(payLoad, 'base64')).toString());
}

let latlng = {
  toName: 'latlng',
  inputs: {
    value: {}
  },
  translateFunction: function (value) {
    return rawTranslate(value).latlng;
  }
};

module.exports = {
  fromName: 'lora-node-FiPy',
  translators: [
    latlng
  ]
};
