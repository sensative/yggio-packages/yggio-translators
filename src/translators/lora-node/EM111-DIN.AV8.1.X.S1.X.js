'use strict';

function getIntFromReg (data, regIdx) {
  let start = 2 + (2 * regIdx);
  let slice = Buffer.concat([data.slice(start + 2, start + 4), data.slice(start, start + 2)]);
  return slice.readInt32BE();
}

function getShortFromReg (data, regIdx) {
  return data.readInt16BE(2 + (2 * regIdx));
}

function rawTranslate (data) {
  data = Buffer.from(data, 'base64');
  return {
    voltage: getIntFromReg(data, 0) / 10,             // V
    current: getIntFromReg(data, 2) / 1000,           // A
    power: getIntFromReg(data, 4) / 10,               // W
    apparentPower: getIntFromReg(data, 6) / 10,       // VA
    reactivePower: getIntFromReg(data, 8) / 10,       // VAR
    powerFactor: getShortFromReg(data, 10) / 1000,    // PF
    frequency: getShortFromReg(data, 11) / 10,        // HZ
    totalEnergy: getIntFromReg(data, 12) / 10,        // kWh
    totalReactivePower: getIntFromReg(data, 14) / 10  // KVArh
  };
}

const simple = {
  toName: 'simple-lora-node',
  inputs: {
    data: {
      path: 'value.data'
    },
    latlng: {
      path: 'latlng'
    }
  },
  translateFunction: function (data, latlng) {
    let obj = {};
    if (data) {
      obj.value = rawTranslate(data);
    }
    if (latlng) {
      obj.latlng = latlng;
    }
    obj.lastActive = Date.now();
    return obj;
  }
};

module.exports = {
  fromName: 'lora-node-EM111-DIN.AV8.1.X.S1.X',
  translators: [
    simple
  ]

};
