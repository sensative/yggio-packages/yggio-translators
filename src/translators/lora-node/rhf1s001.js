'use strict';

function rawTranslate (payload) {
  let newPayload = Buffer.from(payload, 'base64');
  let temp = parseFloat((((175.72 * newPayload.readUInt16LE(1)) / 65536) - 46.85).toFixed(2));
  let humidity = parseFloat((((125 * newPayload.readUInt8(3)) / 256) - 6).toFixed(2));
  let battery = ((newPayload.readUInt8(8) + 150) * 0.01);
  let rssi = (newPayload.readUInt8(6) - 180);
  return {
    temperature: temp,
    humidity: humidity,
    battery: battery,
    rssi: rssi
  };
}

function fiwareTranslate (value, latlng, name, loraNetworkName) {
  let obj = {};
  if (value && value.data) {
    const translated = rawTranslate(value.data);
    obj = {
      dataProvider: loraNetworkName,
      type: 'WeatherObserved',
      name: name,
      dateObserved: Date.now(),
      stationName: value.rxInfo[0].name || '',
      temperature: translated.temperature,
      relativeHumidity: translated.humidity / 100
    };
  }
  if (latlng) {
    obj.location = {
      type: 'Point',
      coordinates: latlng.reverse()
    };
  }
  return obj;
}

const simple = {
  toName: 'simple-lora-node',
  inputs: {
    value: {
      path: 'value.data'
    }
  },
  translateFunction: function (value) {
    let obj = {};
    if (value) {
      obj.value = rawTranslate(value);
    }
    obj.lastActive = Date.now();
    return obj;
  }
};

const encapsulatedFiware = {
  toName: 'encapsulated-fiware-weather-observed',
  inputs: {
    value: {
      path: 'value'
    },
    latlng: {
      path: 'latlng'
    },
    name: {
      path: 'name'
    },
    loraNetworkName: {
      path: 'loraNetworkName'
    }
  },
  translateFunction: function (value, latlng, name, loraNetworkName) {
    return {value: fiwareTranslate(value, latlng, name, loraNetworkName)};
  }
};

const fiware = {
  toName: 'fiware-weather-observed',
  inputs: {
    value: {
      path: 'value'
    },
    latlng: {
      path: 'latlng'
    },
    name: {
      path: 'name'
    },
    loraNetworkName: {
      path: 'loraNetworkName'
    }
  },
  translateFunction: fiwareTranslate
};

module.exports = {
  fromName: 'lora-node-rhf1s001',
  translators: [
    simple,
    encapsulatedFiware,
    fiware
  ]

};
