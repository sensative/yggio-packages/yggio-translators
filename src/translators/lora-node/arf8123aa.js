'use strict';

function rawTranslate (dataBase64) {
  function toDD (degrees, minutes, dir) {
    let dd = degrees + (minutes / 60);
    return dir ? -dd : dd;
  }
  const data = Buffer.from(dataBase64, 'base64');
  let bitmask = data.readUInt8(0);
  let rssiSnrPresent = (bitmask & 0x01);
  let batteryPresent = (bitmask & 0x02);
  let macUpPresent = (bitmask & 0x04);
  let macDownPresent = (bitmask & 0x08);
  let gpsPresent = (bitmask & 0x10);
  // let btnPressed = (bitmask & 0x20);
  // let accelerometer = (bitmask & 0x40);
  let tempPresent = (bitmask & 0x80);
  let offset = 1;
  let temperature, battery, rssi, snr, GPSMetaData, latlng;
  if (tempPresent) {
    temperature = data.readInt8(offset);
    offset++;
  }
  if (gpsPresent) {
    let latDeg = ((data.readUInt8(offset) & 0b11110000) >> 4) * 10;
    latDeg += (data.readUInt8(offset) & 0b00001111);

    let latMinutes = ((data.readUInt8(offset + 1) & 0b11110000) >> 4) * 10;
    latMinutes += (data.readUInt8(offset + 1) & 0b00001111);
    latMinutes += ((data.readUInt8(offset + 2) & 0b11110000) >> 4) / 10;
    latMinutes += (data.readUInt8(offset + 2) & 0b00001111) / 100;
    latMinutes += ((data.readUInt8(offset + 3) & 0b11110000) >> 4) / 1000;

    let lat = toDD(latDeg, latMinutes, (data.readUInt8(offset + 3) & 0x01));

    let lonDeg = ((data.readUInt8(offset + 4) & 0b11110000) >> 4) * 100;
    lonDeg += (data.readUInt8(offset + 4) & 0b00001111) * 10;
    lonDeg += (data.readUInt8(offset + 5) & 0b11110000) >> 4;

    let lonMinutes = (data.readUInt8(offset + 5) & 0b00001111) * 10;
    lonMinutes += ((data.readUInt8(offset + 6) & 0b11110000) >> 4);
    lonMinutes += (data.readUInt8(offset + 6) & 0b00001111) / 10;
    lonMinutes += ((data.readUInt8(offset + 7) & 0b11110000) >> 4) / 100;

    let lon = toDD(lonDeg, lonMinutes, (data.readUInt8(offset + 7) & 0x01));

    let hdop = (data.readUInt8(offset + 8) & 0b11110000) >> 4;
    let satellites = data.readUInt8(offset + 8) & 0b00001111;
    offset += 8;
    GPSMetaData = {
      hdop,
      satellites
    };
    latlng = [lat, lon];
  }
  if (macUpPresent) offset++;
  if (macDownPresent) offset++;
  if (batteryPresent) {
    battery = parseFloat((data.readUInt16BE(offset) / 1000.0).toFixed(3));
    offset += 2;
  }
  if (rssiSnrPresent) {
    rssi = data.readInt8(offset);
    snr = data.readInt8(offset + 1);
    offset += 2;
  }
  return {
    temperature,
    battery,
    rssi,
    snr,
    GPSMetaData,
    latlng
  };
}

const simple = {
  toName: 'simple-lora-node',
  inputs: {
    value: {
      path: 'value.data'
    }
  },
  translateFunction: function (value) {
    let obj = {};
    if (value) {
      const translated = rawTranslate(value);
      obj = {
        value: {
          temperature: translated.temperature,
          battery: translated.battery,
          rssi: translated.rssi,
          snr: translated.snr,
          GPSMetaData: translated.GPSMetaData
        },
        latlng: translated.latlng
      };
    }
    obj.lastActive = Date.now();
    return obj;
  }
};

const fiware = {
  toName: 'fiware-weather-observed',
  inputs: {
    value: {
      path: 'value'
    },
    latlng: {
      path: 'latlng'
    },
    loraNetworkName: {
      path: 'loraNetworkName'
    }
  },
  translateFunction: function (value, latlng, loraNetworkName) {
    let obj = {};
    if (value && value.data) {
      const translated = rawTranslate(value.data);
      obj = {
        dataProvider: loraNetworkName,
        type: 'WeatherObserved',
        dateObserved: Date.now(),
        stationName: value.rxInfo[0].name || '',
        temperature: translated.temperature
      };
    }
    if (latlng) {
      obj.location = {
        type: 'Point',
        coordinates: latlng.reverse()
      };
    }
    return obj;
  }
};

const latlng = {
  toName: 'latlng',
  inputs: {
    value: {}
  },
  translateFunction: function (value) {
    return rawTranslate(value).latlng;
  }
};

module.exports = {
  fromName: 'lora-node-arf8123aa',
  translators: [
    simple,
    fiware,
    latlng
  ]

};
