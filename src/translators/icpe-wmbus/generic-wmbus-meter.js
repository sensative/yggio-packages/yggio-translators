'use strict';

const fiware = {
  toName: 'fiware-device',
  inputs: {
    'data': {
      path: 'value.data',
      validate: {
        type: 'regex',
        filter: '[0-9]'
      }
    },
    'vif': {
      path: 'value.vif',
      validate: {
        type: 'regex',
        filter: '[aA-fF0-9]'
      }
    }
  },
  translateFunction: function (data, vif) {
    var parsedVif = parseInt(vif, 16);
    var parsedData = parseInt(data, 10);
    var unitValue = vif & 0x78; // 0111 1000

    var value = {
      value: 0.0,
      unitText: ''
    };

    if (unitValue === 4) {
      // On Time or Operating Time
      value.value = parsedVif;
      value.unitText = timeUnits[parsedVif & 0x03];
    } else if (unitValue === 11) {
      // Flow Temperature or Return Temperature
      value.value = value.value = parsedData * Math.pow(10, (parsedVif & 0x03) - 3);
      value.unitText = 'C';
    } else if (unitValue === 12) {
      // Temperature Differance or External Temperature
      if (parsedVif & 0x04 === 0) {
        value.unitText = 'K';
      } else {
        value.unitText = 'C';
      }
      value.value = parsedData * Math.pow(10, (parsedVif & 0x03) - 3);
    } else if (unitValue === 13) {
      // Preasure or Time Point
      if (parsedVif & 0x04 === 0) {
        value.value = parsedData * Math.pow(10, (parsedVif & 0x03) - 3);
        value.unitText = 'bar';
      } else {
        if (parsedVif & 0x01 === 0) {
          value.unitText = 'date';
        } else {
          value.unitText = 'time & date';
        }
        value.value = parsedData;
      }
    } else {
      value.value = parsedData * Math.pow(10, (parsedVif & 0x07) - vifTable[unitValue >> 3].sub);
      value.unitText = vifTable[unitValue >> 3].unit;
    }

    return value;
  }
};

var timeUnits = {
  0: 's',
  1: 'm',
  2: 'h',
  3: 'd'
};

var vifTable = {
  0: {
    type: 'Energy',
    unit: 'kWh',
    sub: 6
  },
  1: {
    type: 'Energy',
    unit: 'J',
    sub: 0
  },
  2: {
    type: 'Volume',
    unit: 'm3',
    sub: 6
  },
  3: {
    type: 'Mass',
    unit: 'kg',
    sub: 3
  },
  5: {
    type: 'Power',
    unit: 'W',
    sub: 3
  },
  6: {
    type: 'Power',
    unit: 'J/h',
    sub: 0
  },
  7: {
    type: 'Volume Flow',
    unit: 'm3/h',
    sub: 6
  },
  8: {
    type: 'Volume Flow ext',
    unit: 'm3/min',
    sub: 7
  },
  9: {
    type: 'Volume Flow ext',
    unit: 'm3/s',
    sub: 9
  },
  10: {
    type: 'Mass flow',
    unit: 'kg/h',
    sub: 3
  }
};

module.exports = {
  fromName: 'icpe-wmbus-generic-meter',
  translators: [
    fiware
  ]
};
