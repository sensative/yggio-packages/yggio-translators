'use strict';

const _ = require('lodash');
const Translator = function (translatorMap) {
  _.extend(this, translatorMap);
};
const validator = require('./validator');

Translator.prototype.validate = function (value) {
  let promises = [];
  _.each(this.inputs, function (input) {
    promises.push(validator.validate(input, value));
  });
  return Promise.all(promises);
};
Translator.prototype.translate = function (value) {
  return this.validate(value).then((values) => {
    values.push(value);
    return Promise.resolve(this.translateFunction.apply(null, values));
  });
};
module.exports = Translator;
