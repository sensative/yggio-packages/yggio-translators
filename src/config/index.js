const path = require('path');

module.exports = {
  root: path.normalize(path.join(__dirname, '/../..')),
  env: process.env.NODE_ENV || 'test'
};
